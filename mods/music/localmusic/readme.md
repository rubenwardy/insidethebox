##localmusic

This 'worldmod' is intended to put server specific music in place
and handle event-\>track translation. This allows us to keep music
tracks out of git and have servers place alternative music files
in place without having to bother with git issues.

Simply put all of this in `worldname/worldmods/localmusic` and add
music tracks to the `sounds` subfolder and provide the needed
event to track name translation in the table in `init.lua`.
