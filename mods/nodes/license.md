
`sounds/chest_*.ogg`:
- http://www.freesound.org/people/Sevin7/sounds/269722/ CC0
- http://www.freesound.org/people/Percy%20Duke/sounds/23448/ CC-BY-3.0
- http://www.freesound.org/people/kingsamas/sounds/135576/ CC-BY-3.0
- http://www.freesound.org/people/bulbastre/sounds/126887/ CC-BY-3.0
- http://www.freesound.org/people/Yoyodaman234/sounds/183541/ CC0

`sounds/tnt_explosion.ogg`:
- http://freesound.org/people/shaynecantly/sounds/131555/ CC-BY-3.0 shaynecantly

`sounds/lamp_on.ogg`:
- http://freesound.org/people/mialena24/sounds/364341/ CC0 mialena24

`nodes/models/chest_close.obj`,
`nodes/models/chest_open.obj`,
`nodes/models/flowerpot.obj`: sofar CC-BY-SA-4.0

`nodes/models/stairs_stair.obj`: from `minetest_game`

All textures: From Isabella-II (Or, heavily based upon)
